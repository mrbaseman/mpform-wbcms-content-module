switch(LANGUAGE){
        case 'DE':        
                LANG = {
                        ARE_YOU_SURE : 'Sind Sie sicher?',
                        BACK : 'Zur�ck',
                        RENAME : 'umbenennen',
                        SURE_DELETE_RECORD : 'Soll dieser Datensatz wirklich geloescht werden?',
                        CLOSE: 'schliessen',                
                        CANCEL: 'abbrechen',
                        SAVE: 'speichern',
                        DOUBLECLICK_TO_EDIT: 'Doppelklick, um den Titel umzubenennen',
                        RECORD_NEW_SAVED: 'Datensatz wurde erstellt',
                        SETTINGS_SAVED: 'Einstellungen wurden gespeichert',
                        SUCCESS: 'Erfolgreich',
                        PLEASE_CHOOSE_TYPE: 'bitte Typ auswaehlen',
                        ADD_TITLE: 'Feldtitel hinzuf�gen',                        
                        required_field : 'Pflichtfeld',
                        readonly_field : 'nicht veraenderbar',
                        optional_field : 'freiwillig',
                        disabled_field : 'deaktiviert'
                };
        break;
        case 'EN':
        default:
                LANG = {
                        ARE_YOU_SURE : 'Are you sure?',
                        BACK : 'back',
                        RENAME : 'rename',
                        SURE_DELETE_RECORD : 'Are you sure you want to delete this record?',
                        CLOSE: 'close',
                        CANCEL: 'cancel',
                        SAVE: 'save',
                        DOUBLECLICK_TO_EDIT: 'doubleclick to edit',
                        RECORD_NEW_SAVED: 'New record was added successfully',
                        SETTINGS_SAVED: 'Settings saved successfully',
                        SUCCESS: 'Success',
                        PLEASE_CHOOSE_TYPE: 'please choose type',
                        ADD_TITLE: 'add field title',
                        required_field : 'mandatory',
                        readonly_field : 'read only',
                        optional_field : 'optional',
                        disabled_field : 'disabled'
                };
        break;
}
